'''
Documentation, License etc.

@package label_maker
'''

import labels
import qrcode
import csv
import os.path
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfbase.pdfmetrics import registerFont, stringWidth
from reportlab.graphics import shapes
from reportlab.lib.units import inch
from reportlab.lib import colors
from reportlab.graphics.shapes import Image, Drawing
from reportlab.graphics import renderPDF

import random

import sys
import re

base_path = os.path.dirname(__file__)
registerFont(TTFont('Hack-Bold', os.path.join(base_path, 'Hack-Bold.ttf')))


def gen_qrcode(data):
    qr= qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_H,
        box_size=6,
        border=4,
    )
    qr.add_data(data)
    qr.make(fit=True)
    qr_img = qr.make_image()
    data_path = "output/png/"
    data_path += data
    data_path += ".png"
    qr_img.save(data_path)
    return data_path

def create_label(label,mfpn,description):
    # Measure the width of the name and shrink the font size until it fits.
    font_size = 50
    text_width = width - 10
    name_width = stringWidth(description, "Hack-Bold", font_size)
    while name_width > text_width:
        font_size *= 0.8
        name_width = stringWidth(description, "Hack-Bold", font_size)

    # Write out the name in the centre of the label with a random colour.
    s = shapes.String(width/2.0, 15, description, textAnchor="middle")
    s.fontName = "Hack-Bold"
    s.fontSize = font_size
    s.fillColor = random.choice((colors.black, colors.blue, colors.red, colors.green))
    label.add(s)

def add_qr_code(path,picture_path):
    packet = StringIO.StringIO()
    packet=StringIO.StringIO()
    cv=canvas.Canvas(packet)
    cv.circle(0,10,stroke=1,fill=0)
    cv.save()
    packet.seek(0)
    with open(path,'wb') as fp:
        fp.write(packet.getvalue())
        
def write_name(label, width, height, name):
    # Write the title.

    # Measure the width of the name and shrink the font size until it fits.
    font_size = 35
    text_width = width - 2
    name_width = stringWidth(name[3], "Hack-Bold", font_size)
    while name_width > text_width:
        font_size *= 0.8
        name_width = stringWidth(name[3], "Hack-Bold", font_size)

    # Write out the name in the centre of the label with a random colour.
    s = shapes.String(5, height-25, name[3], textAnchor="start")
    s.fontName = "Hack-Bold"
    s.fontSize = font_size
    s.fillColor = colors.black
    
    label.add(s)
    font_size = 100
    text_width = width - 60 
    name_width = stringWidth(name[4], "Hack-Bold", font_size)
    while name_width > text_width:
        font_size *= 0.8
        name_width = stringWidth(name[4], "Hack-Bold", font_size)

    # Write out the name in the centre of the label with a random colour.
    s = shapes.String(5, height-50, name[4], textAnchor="start")
    s.fontName = "Hack-Bold"
    s.fontSize = font_size
    s.fillColor = colors.black
    label.add(s)


def main(argv):
    digikey_filename = "input.csv"
    

    # Add a couple of labels.
    
    with open(digikey_filename,'rt') as csvfile:
        digkey_parts=csv.reader(csvfile, delimiter=',', quotechar='"')
        for parts in digkey_parts:
            
            parts[3] = re.sub('[\/]','',parts[3])
            background_path = gen_qrcode(parts[3])
            bg = shapes.Drawing(width=210.5, height=297)
            bg_qr_img = Image(150,0,40,200,background_path)
            bg.add(bg_qr_img)
            specs = labels.Specification(75, 29, 1, 1, 75, 29, corner_radius=2,background_image=bg)
            sheet = labels.Sheet(specs, write_name, border=True)
            sheet.add_label(parts)
            label_name = "output/pdfs/"
            label_name += parts[3]
            label_name += ".pdf"
            sheet.save(label_name)        
        
    pass

if __name__ == "__main__":
    main(sys.argv)
